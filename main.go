package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"

	myClient "gitlab.com/fenrirunbound/kube-svcs/internal/client"
	"k8s.io/client-go/kubernetes"
)

var client *kubernetes.Clientset
var defaultNamespace string

func status(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "{\"status\":\"OK\"}")
}

func listServices(w http.ResponseWriter, r *http.Request) {
	services, err := myClient.ListServiceEndpoints(client, "plex")

	if err != nil {
		panic(err.Error())
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(services)
}

func init() {
	var err error

	client, err = myClient.GenerateClient(nil)
	if err != nil {
		panic(err.Error())
	}

	defaultNamespace = os.Getenv("NAMESPACE")
	if defaultNamespace == "" {
		panic("No namespace defined")
	}
}

func main() {
	port := os.Getenv("SERVER_PORT")
	if port == "" {
		port = "8080"
	}

	http.HandleFunc("/api/v1/status", status)
	http.HandleFunc("/api/v1/services", listServices)
	http.ListenAndServe(fmt.Sprintf(":%v", port), nil)
}
