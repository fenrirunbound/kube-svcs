package client

// import "k8s.io/client-go/tools/clientcmd"

import (
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

// GenerateClient Creates a kubernetes client
func GenerateClient(configFilepath *string) (*kubernetes.Clientset, error) {
	var config *rest.Config
	var err error

	if configFilepath != nil {
		config, err = clientcmd.BuildConfigFromFlags("", *configFilepath)
	} else {
		config, err = rest.InClusterConfig()
	}
	if err != nil {
		return nil, err
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}

	return clientset, nil
}

// ListServiceEndpoints ...
func ListServiceEndpoints(client *kubernetes.Clientset, namespace string) ([]v1.Service, error) {
	services, err := client.CoreV1().Services(namespace).List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return services.Items, nil
}
