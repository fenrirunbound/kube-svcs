FROM golang:alpine as builder

RUN apk add --no-cache git \
  && mkdir -p $GOPATH/src/gitlab.com/fenrirunbound

COPY . $GOPATH/src/gitlab.com/fenrirunbound/kube-svcs

RUN cd $GOPATH/src/gitlab.com/fenrirunbound/kube-svcs \
  && go get -u github.com/golang/dep/cmd/dep \
  && dep ensure \
  && CGO_ENABLED=0 go build -a -o goapp \
  && cp goapp /usr/local/bin/goapp

FROM golang:alpine

COPY --from=builder /usr/local/bin/goapp /usr/local/bin/goapp

ENTRYPOINT [ "/usr/local/bin/goapp" ]